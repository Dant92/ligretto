package pl.wroc.pwr.ligretto.model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import pl.wroc.pwr.ligretto.game.Game;

public class LigrettoGUI extends Game {

	Game game;
	JFrame frame = new JFrame();
	JPanel panelGracza = new JPanel();
	JPanel panelPrzeciwnika1 = new JPanel();
	JPanel panelPrzeciwnika2 = new JPanel();
	JPanel panelPrzeciwnika3 = new JPanel();
	JButton row1= new JButton();
	JButton row2= new JButton();
	JButton row3= new JButton();
	JButton przec1Row1= new JButton();
	JButton przec1Row2= new JButton();
	JButton przec1Row3= new JButton();
	JButton przec2Row1= new JButton();
	JButton przec2Row2= new JButton();
	JButton przec2Row3= new JButton();
	JButton przec3Row1= new JButton();
	JButton przec3Row2= new JButton();
	JButton przec3Row3= new JButton();
	JLabel rowLabel = new JLabel("Rzad");
	JLabel przec1RowLabel = new JLabel("Rzad");
	JLabel przec2RowLabel = new JLabel("Rzad");
	JLabel przec3RowLabel = new JLabel("Rzad");
	JLabel handLabel = new JLabel("Reka");
	JLabel przec1HandLabel = new JLabel("Reka");
	JLabel przec2HandLabel = new JLabel("Reka");
	JLabel przec3HandLabel = new JLabel("Reka");
	JButton hand= new JButton();
	JButton przec1Hand= new JButton();
	JButton przec2Hand= new JButton();
	JButton przec3Hand= new JButton();
	JButton searchHandButton= new JButton();
	JButton przec1SearchHandButton= new JButton();
	JButton przec2SearchHandButton= new JButton();
	JButton przec3SearchHandButton= new JButton();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LigrettoGUI gui  = new LigrettoGUI();
		gui.stworzGui();
		gui.prepareCards();
		
	}
	//ctrl+1
	public void stworzGui() {
		
		game =  new Game(3, 10);
		//JButton button = new JButton("Guzik");
		//button.setEnabled(true);
		//frame.getContentPane().add(BorderLayout.NORTH,button);
		
		//panel.setBackground(Color.green);
		frame.getContentPane().add(BorderLayout.SOUTH, panelGracza);
		frame.getContentPane().add(BorderLayout.EAST, panelPrzeciwnika1);
		frame.getContentPane().add(BorderLayout.NORTH, panelPrzeciwnika2);
		frame.getContentPane().add(BorderLayout.WEST, panelPrzeciwnika3);
		
		panelGracza.add(rowLabel);
		panelGracza.add(row1);
		panelGracza.add(row2);
		panelGracza.add(row3);
		panelGracza.add(handLabel);
		panelGracza.add(hand);
		searchHandButton.setText("Przeszukaj reke");
		panelGracza.add(searchHandButton);
		panelPrzeciwnika1.add(przec1RowLabel);
		panelPrzeciwnika1.add(przec1Row1);
		panelPrzeciwnika1.add(przec1Row2);
		panelPrzeciwnika1.add(przec1Row3);
		panelPrzeciwnika1.add(przec1HandLabel);
		panelPrzeciwnika1.add(przec1Hand);
		przec1SearchHandButton.setText("Przeszukaj reke");
		panelGracza.add(przec1SearchHandButton);
		panelPrzeciwnika2.add(przec2RowLabel);
		panelPrzeciwnika2.add(przec2Row1);
		panelPrzeciwnika2.add(przec2Row2);
		panelPrzeciwnika2.add(przec2Row3);
		panelPrzeciwnika2.add(przec2HandLabel);
		panelPrzeciwnika2.add(przec2Hand);
		przec2SearchHandButton.setText("Przeszukaj reke");
		panelGracza.add(przec2SearchHandButton);
		panelPrzeciwnika3.add(przec3RowLabel);
		panelPrzeciwnika3.add(przec3Row1);
		panelPrzeciwnika3.add(przec3Row2);
		panelPrzeciwnika3.add(przec3Row3);
		panelPrzeciwnika3.add(przec3HandLabel);
		panelPrzeciwnika3.add(przec3Hand);
		przec3SearchHandButton.setText("Przeszukaj reke");
		panelGracza.add(przec3SearchHandButton);
		panelPrzeciwnika1.setLayout(new BoxLayout(panelPrzeciwnika1,BoxLayout.Y_AXIS));
		panelPrzeciwnika3.setLayout(new BoxLayout(panelPrzeciwnika3,BoxLayout.Y_AXIS));
		
		
		SrodkowyPanel srodkowyPanel = new SrodkowyPanel();
		frame.getContentPane().add(BorderLayout.CENTER, srodkowyPanel);
		
		
		ActionListener buttonActionListener = new ButtonActionListener();
		searchHandButton.addActionListener(buttonActionListener );
		frame.setTitle("Apka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400,400);
		frame.setVisible(true);
		
	}
	
	public void prepareCards(){
	String b= new String(); 
	int color, color1, color2, color3;
	game.player.prepareCards();
	for (int i =0; i<game.opponents.size();i++){
		game.opponents.get(i).prepareCards();
	}
		przec1Row1.setText(game.opponents.get(0).getCardFromRow(0).getNumber()+1 +"");
		przec2Row1.setText(game.opponents.get(1).getCardFromRow(0).getNumber()+1 +"");
		przec3Row1.setText(game.opponents.get(2).getCardFromRow(0).getNumber()+1 +"");
		row1.setText(game.player.getCardFromRow(0).getNumber()+1 +"");
		color=game.player.getCardFromRow(0).getColor();
		color1=game.opponents.get(0).getCardFromRow(0).getColor();
		color2=game.opponents.get(1).getCardFromRow(0).getColor();
		color3=game.opponents.get(2).getCardFromRow(0).getColor();
		if (color == 0) row1.setBackground(Color.GREEN);
		if (color == 1) row1.setBackground(Color.RED);
		if (color == 2) row1.setBackground(Color.BLUE);
		if (color == 3) row1.setBackground(Color.YELLOW);
		if (color1 == 0) przec1Row1.setBackground(Color.GREEN);
		if (color1 == 1) przec1Row1.setBackground(Color.RED);
		if (color1 == 2) przec1Row1.setBackground(Color.BLUE);
		if (color1 == 3) przec1Row1.setBackground(Color.YELLOW);
		if (color2 == 0) przec2Row1.setBackground(Color.GREEN);
		if (color2 == 1) przec2Row1.setBackground(Color.RED);
		if (color2 == 2) przec2Row1.setBackground(Color.BLUE);
		if (color2 == 3) przec2Row1.setBackground(Color.YELLOW);
		if (color3 == 0) przec3Row1.setBackground(Color.GREEN);
		if (color3 == 1) przec3Row1.setBackground(Color.RED);
		if (color3 == 2) przec3Row1.setBackground(Color.BLUE);
		if (color3 == 3) przec3Row1.setBackground(Color.YELLOW);
		przec1Row2.setText(game.opponents.get(0).getCardFromRow(1).getNumber()+1 +"");
		przec2Row2.setText(game.opponents.get(1).getCardFromRow(1).getNumber()+1 +"");
		przec3Row2.setText(game.opponents.get(2).getCardFromRow(1).getNumber()+1 +"");
		row2.setText(game.player.getCardFromRow(1).getNumber()+1 +"");
		color=game.player.getCardFromRow(1).getColor();
		color1=game.opponents.get(0).getCardFromRow(1).getColor();
		color2=game.opponents.get(1).getCardFromRow(1).getColor();
		color3=game.opponents.get(2).getCardFromRow(1).getColor();
		if (color == 0) row2.setBackground(Color.GREEN);
		if (color == 1) row2.setBackground(Color.RED);
		if (color == 2) row2.setBackground(Color.BLUE);
		if (color == 3) row2.setBackground(Color.YELLOW);
		if (color1 == 0) przec1Row2.setBackground(Color.GREEN);
		if (color1 == 1) przec1Row2.setBackground(Color.RED);
		if (color1 == 2) przec1Row2.setBackground(Color.BLUE);
		if (color1 == 3) przec1Row2.setBackground(Color.YELLOW);
		if (color2 == 0) przec2Row2.setBackground(Color.GREEN);
		if (color2 == 1) przec2Row2.setBackground(Color.RED);
		if (color2 == 2) przec2Row2.setBackground(Color.BLUE);
		if (color2 == 3) przec2Row2.setBackground(Color.YELLOW);
		if (color3 == 0) przec3Row2.setBackground(Color.GREEN);
		if (color3 == 1) przec3Row2.setBackground(Color.RED);
		if (color3 == 2) przec3Row2.setBackground(Color.BLUE);
		if (color3 == 3) przec3Row2.setBackground(Color.YELLOW);
		row3.setText(game.player.getCardFromRow(2).getNumber()+1 +"");
		przec1Row3.setText(game.opponents.get(0).getCardFromRow(2).getNumber()+1 +"");
		przec2Row3.setText(game.opponents.get(1).getCardFromRow(2).getNumber()+1 +"");
		przec3Row3.setText(game.opponents.get(2).getCardFromRow(2).getNumber()+1 +"");
		color=game.player.getCardFromRow(2).getColor();
		color1=game.opponents.get(0).getCardFromRow(2).getColor();
		color2=game.opponents.get(1).getCardFromRow(2).getColor();
		color3=game.opponents.get(2).getCardFromRow(2).getColor();
		if (color == 0) row3.setBackground(Color.GREEN);
		if (color == 1) row3.setBackground(Color.RED);
		if (color == 2) row3.setBackground(Color.BLUE);
		if (color == 3) row3.setBackground(Color.YELLOW);
		if (color1 == 0) przec1Row3.setBackground(Color.GREEN);
		if (color1 == 1) przec1Row3.setBackground(Color.RED);
		if (color1 == 2) przec1Row3.setBackground(Color.BLUE);
		if (color1 == 3) przec1Row3.setBackground(Color.YELLOW);
		if (color2 == 0) przec2Row3.setBackground(Color.GREEN);
		if (color2 == 1) przec2Row3.setBackground(Color.RED);
		if (color2 == 2) przec2Row3.setBackground(Color.BLUE);
		if (color2 == 3) przec2Row3.setBackground(Color.YELLOW);
		if (color3 == 0) przec3Row3.setBackground(Color.GREEN);
		if (color3 == 1) przec3Row3.setBackground(Color.RED);
		if (color3 == 2) przec3Row3.setBackground(Color.BLUE);
		if (color3 == 3) przec3Row3.setBackground(Color.YELLOW);
	}

	
}
