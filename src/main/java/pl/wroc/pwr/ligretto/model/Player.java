package pl.wroc.pwr.ligretto.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Player {

	private int sizeOfHand;
	private CardSet hand;
	private List<Card> stack = new ArrayList <Card>();
	private List <Card> row = new ArrayList <Card>();
	private Card temp= new Card();
	
	public Player()
	{
		hand = new CardSet();
		sizeOfHand = 40;
	}
	
	public Player(int n){
	hand = new CardSet(n);
	sizeOfHand = 4*n;
	}
	
	public void addToHand(Card card){
		temp=card;
	}
	
	public void addToRow(Card card){
		this.row.add(card);
	}
	
	public void prepareCards(){
		this.hand.shuffle();
		for (int i =0 ; i< 10; i++)
		{
			this.stack.add(this.hand.popCard());
		}
		for (int i= 0; i< 3; i++)
		{
			this.row.add(this.hand.popCard());
		}
		sizeOfHand = sizeOfHand - 13;
	}
	
	public void showCardsInHand(){
		hand.showAllCards();
	}
	
	public void showCardsInStack(){
		for(int i=0; i<this.stack.size();i++)
		{
			this.stack.get(i).showCard();
		}
		System.out.println();
	}
	
	public void showCardsInRow()
	{
		for(int i =0; i<this.row.size(); i++){
			this.row.get(i).showCard();
		}
		System.out.println();
			
	}
	
	public Card popCardFromRow(int n){
		Card card;
		card = this.row.get(n-1);
		this.row.remove(n-1);
		return card;
	}
	
	public Card popCardFromStack(int n){
		Card card;
		card = this.stack.get(n-1);
		this.stack.remove(n-1);
		return card;
	}
	
	public void takeCardFromStackToRow(){
		Card card;
		card = this.popCardFromStack(1);
		this.addToRow(card);
	}
	
	public void searchFromHand(){
		for(int i =0; i< 3; i++){
			hand.addCardToSet(hand.popCard(1));
			}
		System.out.println("Karta która znalazles:");
		hand.showCardFromSet(1);
		this.temp=hand.popCard(1);		
	}
	
	public Card popFromHand()
	{
		Card card;
		card = temp;
		temp.setNumber(0);
	return card;
	}
	
	public void notThisCard()
	{
		hand.addCardToSet(temp);
	}
	
	public void shuffleHand(){
		hand.shuffle();
	}
	
	public void shuffleStack(){
		Collections.shuffle(stack);
	}
	
	public int getSizeOfStack(){
		return this.stack.size();
	}


}
