package pl.wroc.pwr.ligretto.model;

public class Card implements Comparable<Card> {

	private int number;
	private int color;

	public Card()
	{
		this.number = 0;
	}
	
	public Card(int number){
		this.number = number;
	}
	
	public Card(int number, int color){
		this.number = number;
		this.color = color;
	}
	
	public int compareTo(Card o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getColor(){
		return this.color;
	}
	
	public int getNumber(){
		return this.number;
	}

	public void setNumber(int n){
		this.number=0;
	}
	
	public void showCard(){
	
		System.out.print(this.number+1 +"k"+ (this.color+1) +" ");
	}
}
