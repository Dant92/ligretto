package pl.wroc.pwr.ligretto.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CardSet {

	private List<Card> cards = new ArrayList<Card>();
	private int numberOfCards;
	
	public CardSet(){
		numberOfCards = 10;
	for (int j = 0; j < 4; j++){
		for ( int i = 0 ; i< numberOfCards; i++){
			Card card = new Card(i,j);
			cards.add(card);
		}
		}
	}
	
	public CardSet(int n){
		numberOfCards = n;
		for (int j = 0; j < 4; j++){
			for ( int i = 0 ; i< numberOfCards; i++){
				Card card = new Card(i,j);
				cards.add(card);
			}
			}
		}
	
	public CardSet(int n, int k){
		numberOfCards = n;
			for ( int i = 0 ; i< numberOfCards; i++){
				Card card = new Card(i,k);
				cards.add(card);
			}
		}
	
	public void shuffle(){
		Collections.shuffle(cards);
	}
	
	public Card popCard(){
		Card card;
		card = this.cards.get(cards.size()-1);
		this.cards.remove(cards.size()-1);
		return card;
	}
	
	public Card popCard(int i){
		Card card;
		card = this.cards.get(i-1);
		this.cards.remove(i-1);
		return card;
	}
	
	public void showAllCards(){
		for ( int i=0; i< cards.size(); i++){
			this.cards.get(i).showCard();
		}
		System.out.println();
	}
	
	public void showCardFromSet(int n){
		cards.get(n-1).showCard();
		System.out.println();
		
	}
	
	public void addCardToSet(Card card){
		cards.add(card);
	}
	
}
