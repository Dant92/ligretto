package pl.wroc.pwr.ligretto.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pl.wroc.pwr.ligretto.model.LigrettoGUI;
import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.Player;

public class Game {
	
	public Player player;
	public List<Player> opponents= new ArrayList<Player>();
	private static int numberOfopponents;
	private static int numberOfCardsInCardSet;
	private int ifWinGame;
	private List<Card> stacks = new ArrayList<Card>();
	
	public Game() {
		numberOfopponents = 3;
		this.player = new Player();
		for (int i =0 ; i < 3 ; i++)
		{
			this.addPlayer();
		}
	}
	
	private void addPlayer() {
		Player opponent = new Player();
		this.opponents.add(opponent);
	}

	private void addPlayer(int n) {
		Player opponent = new Player(n);
		this.opponents.add(opponent);
	}
	
	public Game(int numberOfopponents){
		for (int i =0 ; i < numberOfopponents ; i++)
		{
			this.addPlayer();
		}
		
	}
	
	public Game(int numberOfopponents, int numberOfCardsInCardSet){
		for (int i =0 ; i < numberOfopponents ; i++)
		{
			this.addPlayer(numberOfCardsInCardSet);
		}
		this.player = new Player (numberOfCardsInCardSet);
	}
	
	public void showStacks(){
		for (int i =0; i<this.stacks.size();i++){
			if (this.stacks.get(i).getNumber()!=numberOfCardsInCardSet)
			stacks.get(i).showCard();
		}
		System.out.println();
	}
	
	public int addStack(Card card){
		if(card.getNumber()==1){
			stacks.add(card);
			return 1;
		}
		else {
			System.out.println("Karta nie jest jedynka");
			return 0;
		}
	}
	
	public void makeTurn(Player player){
		int assertion =1;
		int what=0;
		int whichCard=0, whichStack =0;
		Card card= new Card(0);
		System.out.println("Twoj ruch: 0 - Szukaj , 1 - Doloz z rzedu, 2 - doloz z reki"
				+" 3 - Stworz nowy kopiec");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine ();
		what=Integer.parseInt(input);
		
		
		// czytanie what
		if (what==0) {
			card = player.popFromHand();
			if ( card.getNumber()!=0) {
			 player.notThisCard();
			 player.searchFromHand();
			}
			else {player.searchFromHand();
			//player.notThisCard();
			}
		}
		if (what==1) {
			 //player.notThisCard();
			System.out.println("PRosze podac ktora karta i na ktory kopiec dodac (od lewej");
			// Czytanie whichCard i whichStack + assercje
			 input = scanner.nextLine ();
			whichCard=Integer.parseInt(input);
			input = scanner.nextLine ();
			whichStack=Integer.parseInt(input);
			card=player.popCardFromRow(whichCard);
			assertion=this.addCardToStack(card, whichStack);
			if (assertion == 0) player.addToRow(card);
			else player.takeCardFromStackToRow();
		}
		if (what ==2) {
			// player.notThisCard();
			System.out.println("Na ktory kopiec dodac?");
		// czytanie ktory whichStack +assercje
			input = scanner.nextLine ();
			whichStack=Integer.parseInt(input);
			card = player.popFromHand();
			if ( card.getNumber()==0) System.out.println("Nie masz nic na rece");
			else {
				assertion=this.addCardToStack(card, whichStack);
				if (assertion ==0) player.addToHand(card);
			}
			
		}
		if (what ==3){
			int addFromWhere=0;
			System.out.println("Z reki - 0 , z rzedu -1");
			// czytanie addFromWhere +assercje
			input = scanner.nextLine ();
			addFromWhere=Integer.parseInt(input);
			if (addFromWhere==1){
				// player.notThisCard();
				System.out.println("O ktora karte chodzi (od lewej) ?");
				// czytanie whichCard + assercje
				input = scanner.nextLine ();
				whichCard=Integer.parseInt(input);
				card= player.popCardFromRow(whichCard);
				assertion = this.addStack(card);
				if ( assertion == 1) player.addToRow(card);
				else player.takeCardFromStackToRow();
			}
			if (addFromWhere==0){
				//card=player.popFromHand();
				if(card.getNumber()!=0){
					assertion = this.addStack(card);
					if (assertion ==0) player.addToHand(card); 
				}
				else System.out.println("Nie masz nic na rece");
			}
			
		}
	
	}
	
	public int addCardToStack(Card card, int n){
		if(this.stacks.get(n-1).getColor()==card.getColor() &&
				this.stacks.get(n-1).getNumber()==card.getNumber()-1){
			this.stacks.remove(n-1);
			this.stacks.add(n-1, card); // ???
			return 1;
			}
		else {
			System.out.println("Zla operacja, nieprawidlowy karta");
			return 0;
		}
	}
	
	public void clearConsole(){
		try 
		{
			Runtime.getRuntime().exec("cls/clear");
			}
		catch (final Exception e)
		{
			System.out.println("Blad konsoli");
		}
		
	}
	
	public void endOfGame(Player player){
		if (player.getSizeOfStack()==0) this.ifWinGame = 0;
		
	}
	
	public static void main(String[] args) {
		
		
		Game game;
		
		List<Card> tmp = new ArrayList<Card>(); 
		System.out.println(" Prosze podac ilosc przeciwnikow");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine ();
		System.out.println(" Prosze podac ilosc w talii kart");
		String input2 = scanner.nextLine ();
		//scanner.close();
		
		try{
			numberOfopponents = Integer.parseInt(input);
			numberOfCardsInCardSet = Integer.parseInt(input2);
			game =  new Game(numberOfopponents, numberOfCardsInCardSet);
			 
		}
		catch(NumberFormatException e){
		System.out.println(e);
		game = new Game();
		}
		game.ifWinGame = 10;
		game.player.prepareCards();
		//tmp=game.player.getWholeRow();
		
		//LigrettoGUI gui  = new LigrettoGUI();
		//gui.stworzGui();
		//gui.prepareCards(tmp);
		while (game.ifWinGame > 0){
			System.out.println("Karty na stole");
			game.showStacks();
			System.out.println("Twoje Karty:");
			game.player.showCardsInRow();
			game.makeTurn(game.player);
			game.clearConsole();
			//game.endOfGame(game.player);	
		}
		
			
		

	}
	
}
